Vue.filter('doneLabel', function (value) {
    if (!value) {
        return "Não Paga";
    }
    return "Paga";
});

Vue.filter('statusGeneral', function (value) {
    if (value === false) {
        return "Nenhuma conta cadastrada";
    }
    if (!value) {
        return "Nenhuma conta a pagar";
    }
    return "Existe(m) " + value + " conta(s) a ser(em) paga(s)";
});

var app = new Vue({
    el: "#app",
    data: {
        title: "Contas a receber",
        menus: [
            { id: 0, name: "Listar Contas" },
            { id: 1, name: "Criar Conta" }
        ],
        activedView: 0,
        formType: "insert",
        bill: {
            date_due: "",
            name: "",
            value: 0,
            done: false
        },
        names: [
            'CPFL',
            'SABESP',
            'Gasolina',
            'Alimentação',
            'Refeição',
            'Laser'
        ],
        bills: [
            { date_due: "01/06/2016", name: "CPFL", value: 86.99, done: true },
            { date_due: "01/07/2016", name: "CPFL", value: 87.99, done: false },
            { date_due: "01/08/2016", name: "CPFL", value: 88.00, done: false }
        ]
    },
    computed: {
        status: function() {
            if (!this.bills.length)
                return false;

            var count = 0;
            for (var i in this.bills) {
                if (!this.bills[i].done) {
                    count++;
                }
            }
            return count;
        }
    },
    methods: {
        showView: function (id) {
            this.activedView = id;
            if (id == 1) {
                this.formType = "insert";
            }
        },
        submit: function () {
            if (this.formType == "insert") {
                this.bills.push(this.bill);
            }

            this.bill = {
                date_due: "",
                name: "",
                value: 0,
                done: false
            };

            this.activedView = 0;
        },
        loadBill: function (bill) {
            this.bill = bill;
            this.activedView = 1;
            this.formType = "update";
        },
        deleteBill: function (bill) {
            if (confirm('Deseja excluir esta conta?')) {
                this.bills.$remove(bill)
            }
        }
    }
});